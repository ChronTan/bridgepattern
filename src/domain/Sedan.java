package domain;

public class Sedan extends Car {
    public Sedan(Make make) {
        super(make);
    }

    @Override
    void showDetails() {
        System.out.println("Sedan");
        make.setMake();
    }
}
