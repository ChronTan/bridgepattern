package domain;

public class Hatchback extends Car {
    public Hatchback(Make make) {
        super(make);
    }

    @Override
    void showDetails() {
        System.out.println("Hatchback");
        make.setMake();
    }
}
